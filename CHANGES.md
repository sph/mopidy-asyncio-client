# Upgrade from `mopidy-async-client` to `mopidy-async`___`io`___`-client`

## Incompatible changes

The package changed name from `mopidy-async-client` to
`mopidy-asyncio-client`!

`MopidyClient` lost the `url` parameter, but gained the `host` and `port`
parameters instead.

`MopidyClient`'s `.listener` property was renamed to the more appropriate
`.eventhandler`. But it also gained `bind()`, `unbind()` and `clear()`
methods, which are proxies to the event handler's methods. So please use them
instead.

Deprecation warnings:
- `TracklistController.add(tracks)`: Use the `uris` parameter.
- `TracklistController.eot_track()`: Use the `get_eot_tlid()` method.
- `TracklistController.next_track()`: Use the `get_next_tlid()` method.
- `TracklistController.previous_track()`: Use the `get_previous_track()`
  method.
- `PlaybackController.play(tl_track)`: Use the `tlid` parameter.

`MopidyListener` renamed to the more appropriate `MopidyEventHandler`.


## Important changes

- `MopidyClient` understands the `reconnect_attempts=None` keyword parameter;
  this results in infinite connection attempts.
  ([`mopidy-async-client` PR#1](https://github.com/SvineruS/mopidy-async-client/pull/1))
- Downgrade timeout errors, if a request is not answered in time. (Used to log
  an exception.)
  ([`mopidy-async-client` PR#3](https://github.com/SvineruS/mopidy-async-client/pull/3))


## Bugs

- `MopidyListener.clear()` did not set `.bindings` to
  `collections.defaultdict(list)`.
  ([`mopidy-async-client` PR#2](https://github.com/SvineruS/mopidy-async-client/pull/3))
- `ResponseMessage` did not check, if `._on_result` is set.


## Internal changes

- No f-strings in log messages.
  ([`mopidy-async-client` PR#4](https://github.com/SvineruS/mopidy-async-client/pull/4))
- Renaming of some variables
- Conformation to pep8.
- Better documentation.
- Better log messages.
- Use `logger` instead of `logging` throughout.
- (Massive) code clean-up:
    - Removed one or the other imported module
    - Clean up `try: except:` blocks.
    - Make `ResponseMessage` a proper, instantiable class (and use it as such).
    - Replace `assert` statements, because they are not executed, when run in
      optimization mode.
    - Clean up return values
- Remove the `examples/` directory, because the example in the
  [`README.md`](file://README.md) file covers all those examples.


## Licence
- Add copyright notices for all previous authors.


## Version number
- Main version (first digit) follows `Mopidy`'s version number.
