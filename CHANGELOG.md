# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/).


## [Unreleased]

### Added

### Changed

### Deprecated

### Removed

### Fixed

### Security


## [3.1.0] - 2021-08-02

### Changed
- License changed to GPLv2+, because the code which this is based on is
  licensed under GPLv2.


## [3.0.1] - 2021-07-26

### Fixed
- Fixes connection problems, when the network is not available.


## [3.0.0] - 2021-04-28

First forked release. See [CHANGES](CHANGES.md).
